package com.pooh.guess100

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    var secrectNum = SecretNumber(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        text_show.text = secrectNum.showStartEnd()
        text_answer.text = secrectNum.answerNumver.toString()
        text_guessNum.text = secrectNum.counter.toString()

        text_bestNum.text = secrectNum.bestDisplay
        bt_guess.setOnClickListener{
            secrectNum.inputNum = ed_inputNum.text.toString().toInt()
            AlertDialog.Builder(this)
                .setTitle("結果")
                .setMessage(secrectNum.message)
                .setNeutralButton("再猜一次",null)
                .show()
            ed_inputNum.text.clear()
            text_show.text = secrectNum.showStartEnd()
            text_guessNum.text = secrectNum.counter.toString()
            text_bestNum.text = secrectNum.bestDisplay
        }
    }
}
