package com.pooh.guess100

import android.content.Context
import kotlin.random.Random

class SecretNumber(val context:Context) {
    var start = 1
    var end = 100
    var counter = 0
    var best :Int = -1
    var bestDisplay:String = "無紀錄"
        get() {
            best = context.getSharedPreferences("guess", Context.MODE_PRIVATE)
                .getInt("BEST",-1)
            field = if (best==-1) "無紀錄" else best.toString()
            return field
        }
    lateinit var message:String
    var inputNum:Int = -1
        set(value) {
            counter++
            message = when{
                value > end || value < start -> "輸入數字範圍錯誤!"
                value > answerNumver -> {
                    end = value
                    "猜的數字太大了!"
                }
                value < answerNumver -> {
                    start = value
                    "猜的數字太小了!"
                }
                else -> {
                    if (best == -1 || best > counter){
                        context.getSharedPreferences("guess", Context.MODE_PRIVATE)
                            .edit()
                            .putInt("BEST",counter)
                            .apply()
                    }
                    var temp = answerNumver
                    start = 1
                    end = 100
                    answerNumver = Random.nextInt(end-start+1)+start
                    bestDisplay = bestDisplay
                    "恭喜你猜中了，數字 $temp ，共猜 $counter 次!"
                }
            }
        }
    var answerNumver = Random.nextInt(end-start+1)+start
    fun showStartEnd():String{
        return " $start 到 $end "
    }
}